package ru.buzanov.tm.bootstrap;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.reflections.Reflections;
import ru.buzanov.tm.api.ServiceLocator;
import ru.buzanov.tm.api.service.ITerminalService;
import ru.buzanov.tm.command.AbstractCommand;
import ru.buzanov.tm.endpoint.*;
import ru.buzanov.tm.util.TerminalService;

import java.io.IOException;
import java.lang.Exception;
import java.net.URL;
import java.util.Collection;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

@Getter
@Setter
@NoArgsConstructor
public final class Bootstrap implements ServiceLocator {
    @NotNull
    private final String adress = "http://0.0.0.0:8080/";
    @NotNull
    private final ITerminalService terminalService = new TerminalService();
    @NotNull
    private final Map<String, AbstractCommand> commands = new TreeMap<>();
    @NotNull
    private final Set<Class<? extends AbstractCommand>> classes = new Reflections("ru.buzanov.tm").getSubTypesOf(AbstractCommand.class);

    @Nullable
    private ProjectEndpoint projectEndpoint;
    @Nullable
    private TaskEndpoint taskEndpoint;
    @Nullable
    private UserEndpoint userEndPoint;
    @Nullable
    private SessionEndpoint sessionEndpoint;
    @Nullable
    private AdminUserEndpoint adminUserEndpoint;
    @Nullable
    private Session currentSession;
    @Nullable
    private User currentUser;

    @SneakyThrows
    private void init() {
        commands.clear();
        AbstractCommand command;
        for (Class aClass : classes) {
            command = (AbstractCommand) aClass.newInstance();
            command.setServiceLocator(this);
            commands.put(command.command(), command);
        }
    }

    public void connect() {
        while (true) {
            try {
                terminalService.printLineG("ENTER IP ADRESS");
                String ip = terminalService.readLine();
                if (ip.isEmpty())
                    ip = "localhost:8080";
                URL urlProject = new URL("http://" + ip + "/ProjectEndpoint?wsdl");
                URL urlTask = new URL("http://" + ip + "/TaskEndpoint?wsdl");
                URL urlUser = new URL("http://" + ip + "/UserEndpoint?wsdl");
                URL urlSession = new URL("http://" + ip + "/SessionEndpoint?wsdl");
                URL urlAdminUser = new URL("http://" + ip + "/AdminUserEndpoint?wsdl");

            setProjectEndpoint(new ProjectEndpointService(urlProject).getProjectEndpointPort());
            setTaskEndpoint(new TaskEndpointService(urlTask).getTaskEndpointPort());
            setUserEndPoint(new UserEndpointService(urlUser).getUserEndpointPort());
            setSessionEndpoint(new SessionEndpointService(urlSession).getSessionEndpointPort());
            setAdminUserEndpoint(new AdminUserEndpointService(urlAdminUser).getAdminUserEndpointPort());
            } catch (Exception e) {
                e.printStackTrace();
            }
            if (sessionEndpoint!=null)
                break;
        }
        terminalService.printLineG("CONNECTED");
        init();
    }

    public void start() {
        connect();

        @NotNull String command = "";

        terminalService.printLineG("***WELCOME TO TASK MANAGER***");
        terminalService.printLine("Type help to see command list.");
        while (!commands.get("exit").command().equals(command)) {
            try {
                command = terminalService.readLine();
                if (!command.isEmpty()) {

                    if (getCurrentSession() == null && commands.get(command).isSecure())
                        terminalService.printLineR("Authorise, please");
                    else {
                        if (getCurrentUser() != null && !commands.get(command).isRoleAllow(getCurrentUser().getRoleType()))
                            terminalService.printLineR("This command is not allow for you");
                        else
                            commands.get(command).execute();
                    }
                }

            } catch (Exception | ru.buzanov.tm.endpoint.Exception e) {
                terminalService.printLineR(e.getMessage(), "  ", e.toString());
            }
        }
    }

    @NotNull
    public Collection<AbstractCommand> getCommands() {
        return commands.values();
    }
}