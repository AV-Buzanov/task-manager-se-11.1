package ru.buzanov.tm.api;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.buzanov.tm.api.service.ITerminalService;
import ru.buzanov.tm.command.AbstractCommand;
import ru.buzanov.tm.endpoint.*;

import java.util.Collection;

public interface ServiceLocator {

    @NotNull Collection<AbstractCommand> getCommands();

    @NotNull ITerminalService getTerminalService();

    @NotNull ProjectEndpoint getProjectEndpoint();

    @NotNull TaskEndpoint getTaskEndpoint();

    @NotNull UserEndpoint getUserEndPoint();

    @NotNull SessionEndpoint getSessionEndpoint();

    @NotNull AdminUserEndpoint getAdminUserEndpoint();

    @Nullable Session getCurrentSession();

    @Nullable User getCurrentUser();

    void connect();

    void setCurrentSession(@Nullable Session session);

    void setCurrentUser(@Nullable User user);

    void setProjectEndpoint(@NotNull ProjectEndpoint projectEndpoint);

    void setTaskEndpoint(@NotNull TaskEndpoint taskEndpoint);

    void setUserEndPoint(@NotNull UserEndpoint userEndPoint);

    void setSessionEndpoint(@NotNull SessionEndpoint sessionEndpoint);

    void setAdminUserEndpoint(@NotNull AdminUserEndpoint adminUserEndpoint);
}
