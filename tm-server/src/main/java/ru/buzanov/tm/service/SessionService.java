package ru.buzanov.tm.service;

import ru.buzanov.tm.api.repository.IRepository;
import ru.buzanov.tm.api.service.ISessionService;
import ru.buzanov.tm.entity.Session;

public class SessionService extends AbstractService<Session> implements ISessionService {
    public SessionService(IRepository<Session> abstractRepository) {
        super(abstractRepository);
    }
}
