package ru.buzanov.tm.service;

import lombok.NoArgsConstructor;
import org.apache.commons.codec.digest.DigestUtils;
import org.jetbrains.annotations.Nullable;
import ru.buzanov.tm.api.repository.IUserRepository;
import ru.buzanov.tm.api.service.IUserService;
import ru.buzanov.tm.entity.User;
import ru.buzanov.tm.enumerated.RoleType;

import java.util.Collection;

@NoArgsConstructor
public class UserService extends AbstractService<User> implements IUserService {
    private IUserRepository userRepository;
    private User currentUser;

    public UserService(final IUserRepository userRepository) {
        super(userRepository);
        this.userRepository = userRepository;
    }

    @Override
    @Nullable
    public User load(@Nullable final User user) {
        if (user == null)
            return null;
        if (user.getPasswordHash()!=null)
        user.setPasswordHash(DigestUtils.md5Hex(user.getPasswordHash()));
        return userRepository.load(user);
    }

    @Override
    public void merge(@Nullable final String id, @Nullable final User user) {
        if (id == null || user == null || id.isEmpty())
            return;
        if (user.getPasswordHash()!=null)
        user.setPasswordHash(DigestUtils.md5Hex(user.getPasswordHash()));
        userRepository.merge(id, user);
    }

    @Nullable
    public User findByLogin(@Nullable final String login) {
        if (login == null || login.isEmpty())
            return null;
        return userRepository.findByLogin(login);
    }

    public boolean isPassCorrect(@Nullable final String login, @Nullable final String pass) {
        if (login == null || login.isEmpty())
            return false;
        if (pass == null || pass.isEmpty())
            return false;
        if (!isLoginExist(login))
            return false;
        return userRepository.isPassCorrect(login, DigestUtils.md5Hex(pass));
    }

    public boolean isLoginExist(@Nullable final String login) {
        if (login == null || login.isEmpty())
            return false;
        return userRepository.isLoginExist(login);
    }

    @Override
    @Nullable
    public User getCurrentUser() {
        return currentUser;
    }

    @Override
    public void setCurrentUser(@Nullable final User currentUser) {
        this.currentUser = currentUser;
    }

    @Nullable
    public Collection<User> findByRole(@Nullable final RoleType role) {
        if (role == null)
            return null;
        return userRepository.findByRole(role);
    }
}