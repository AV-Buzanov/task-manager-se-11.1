package ru.buzanov.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.buzanov.tm.api.repository.IWBSRepository;
import ru.buzanov.tm.constant.FormatConst;
import ru.buzanov.tm.entity.AbstractWBS;

import java.util.ArrayList;
import java.util.Collection;

public abstract class AbstractWBSRepository<T extends AbstractWBS> extends AbstractRepository<T> implements IWBSRepository<T> {
    @NotNull
    public Collection<T> findAll(@NotNull final String userId) {
        Collection<T> list = new ArrayList<>();
        for (T entity : map.values()) {
            if (userId.equals(entity.getUserId()))
                list.add(entity);
        }
        return list;
    }

    @NotNull
    public Collection<T> findByName(@NotNull final String userId, @NotNull final String name) {
        Collection<T> list = new ArrayList<>();
        for (T entity : findAll(userId)) {
            if (entity.getName().contains(name))
                list.add(entity);
        }
        return list;
    }

    @Nullable
    public T findName(@NotNull final String userId, @NotNull final String name) {
        for (T entity : findAll(userId)) {
            if (entity.getName().equals(name))
                return entity;
        }
        return null;
    }

    @NotNull
    public Collection<T> findByDescription(@NotNull final String userId, @NotNull final String desc) {
        Collection<T> list = new ArrayList<>();
        for (T entity : findAll(userId)) {
            if (entity.getDescription() != null && entity.getDescription().contains(desc))
                list.add(entity);
        }
        return list;
    }

    @Nullable
    public T findOne(@NotNull final String userId, @NotNull final String id) {
        if (map.containsKey(id) && userId.equals(map.get(id).getUserId()))
            return map.get(id);
        else return null;
    }

    public boolean isNameExist(@NotNull final String userId, @NotNull final String name) {
        for (T entity : findAll(userId)) {
            if (name.equals(entity.getName()))
                return true;
        }
        return false;
    }

    @NotNull
    public String getList() {
        int indexBuf = 1;
        StringBuilder s = new StringBuilder();
        for (T entity : findAll()) {
            s.append(indexBuf).append(". ").append(entity.getName());
            if (findAll().size() > indexBuf)
                s.append(System.getProperty("line.separator"));
            indexBuf++;
        }
        return s.toString();
    }

    @NotNull
    public String getList(@NotNull final String userId) {
        if (findAll(userId).isEmpty())
            return FormatConst.EMPTY_FIELD;
        int indexBuf = 1;
        StringBuilder s = new StringBuilder();
        for (T entity : findAll(userId)) {
            s.append(indexBuf).append(". ").append(entity.getName());
            if (findAll(userId).size() > indexBuf)
                s.append(System.getProperty("line.separator"));
            indexBuf++;
        }
        return s.toString();
    }

    @Nullable
    public String getIdByCount(final int count) {
        int indexBuf = 1;
        for (T entity : findAll()) {
            if (indexBuf == count)
                return entity.getId();
            indexBuf++;
        }
        return null;
    }

    @Nullable
    public String getIdByCount(@NotNull final String userId, final int count) {
        int indexBuf = 1;
        for (T entity : findAll(userId)) {
            if (indexBuf == count)
                return entity.getId();
            indexBuf++;
        }
        return null;
    }

    public abstract void merge(@NotNull final String userId, @NotNull final String id, @NotNull final T entity);

    @Nullable
    public T remove(@NotNull final String userId, @NotNull final String id) {
        if (userId.equals(map.get(id).getUserId()))
            return map.remove(id);
        return null;
    }

    public void removeAll(@NotNull final String userId) {
        for (T entity : findAll(userId))
            remove(entity.getId());
    }
}
