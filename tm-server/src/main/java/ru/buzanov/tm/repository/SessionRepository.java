package ru.buzanov.tm.repository;

import org.jetbrains.annotations.NotNull;
import ru.buzanov.tm.api.repository.ISessionRepository;
import ru.buzanov.tm.entity.Session;

public class SessionRepository extends AbstractRepository<Session> implements ISessionRepository {
    @Override
    public void merge(@NotNull String id, @NotNull Session session) {
        if (session.getCreateDate() != null)
            map.get(id).setCreateDate(session.getCreateDate());
        if (session.getSignature() != null)
            map.get(id).setSignature(session.getSignature());
        if (session.getUserId() != null)
            map.get(id).setUserId(session.getUserId());
    }
}
