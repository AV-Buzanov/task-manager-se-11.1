package ru.buzanov.tm.constant;

public class HashConst {
    public final static String SALT = "my_salt";
    public final static int CYCLE = 95;
    public final static int SESSION_LIFETIME = 330000;
}
