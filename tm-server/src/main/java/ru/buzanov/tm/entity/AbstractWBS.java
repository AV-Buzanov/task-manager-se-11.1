package ru.buzanov.tm.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.buzanov.tm.enumerated.Status;

import java.util.Date;

@Getter
@Setter
@NoArgsConstructor
public abstract class AbstractWBS extends AbstractEntity {
    @Nullable private String name;
    @Nullable private String userId;
    @NotNull private Status status = Status.PLANNED;
    @NotNull private Date createDate = new Date(System.currentTimeMillis());
    @Nullable private Date startDate;
    @Nullable private Date finishDate;
    @Nullable private String description;
}