package ru.buzanov.tm.entity;

import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;

@Getter
@Setter
@NoArgsConstructor
@JsonPropertyOrder({"id", "name", "startDate", "finishDate", "description", "status", "createDate"})
public class Task extends AbstractWBS {
    @Nullable private String projectId;
}