package ru.buzanov.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.buzanov.tm.entity.User;
import ru.buzanov.tm.enumerated.RoleType;

import java.util.Collection;

public interface IUserRepository extends IRepository<User> {

    @Nullable User findByLogin(@NotNull final String login);

    boolean isPassCorrect(@NotNull final String login, @NotNull final String pass);

    boolean isLoginExist(@NotNull final String login);

    @NotNull Collection<User> findByRole(@NotNull final RoleType role);
}
