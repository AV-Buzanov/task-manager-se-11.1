package ru.buzanov.tm.api.service;

import org.jetbrains.annotations.Nullable;
import ru.buzanov.tm.entity.User;
import ru.buzanov.tm.enumerated.RoleType;

import java.util.Collection;

public interface IUserService extends IService<User> {

    @Nullable User findByLogin(@Nullable String login);

    boolean isPassCorrect(@Nullable final String login, @Nullable final String pass);

    boolean isLoginExist(@Nullable final String login);

    @Nullable User getCurrentUser();

    void setCurrentUser(@Nullable final User user);

    @Nullable Collection<User> findByRole(@Nullable final RoleType role);
}
