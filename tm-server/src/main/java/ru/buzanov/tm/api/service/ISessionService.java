package ru.buzanov.tm.api.service;

import ru.buzanov.tm.entity.Session;

public interface ISessionService extends IService<Session> {
}
