package ru.buzanov.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Collection;

public interface IWBSRepository<T> extends IRepository<T> {

    @NotNull Collection<T> findAll(@NotNull final String userId);

    @Nullable T findOne(@NotNull final String userId, @NotNull final String id);

    boolean isNameExist(@NotNull final String userId, @NotNull final String name);

    @NotNull String getList();

    @NotNull String getList(@NotNull final String userId);

    @Nullable String getIdByCount(final int count);

    @Nullable String getIdByCount(final String userId, final int count);

    void merge(@NotNull final String userId, @NotNull final String id, @NotNull final T project);

    @Nullable T remove(@NotNull final String userId, @NotNull final String id);

    void removeAll(@NotNull final String userId);

    @NotNull Collection<T> findByDescription(@NotNull final String userId, @NotNull final String desc);

    @NotNull Collection<T> findByName(@NotNull final String userId, @NotNull final String name);
}
